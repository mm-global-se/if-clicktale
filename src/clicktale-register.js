mmcore.IntegrationFactory.register(
  'ClickTale', {
    validate: function (data) {
      if(!data.campaign)
          return 'No campaign.';
      return true;
    },

    check: function (data) {
        return (window.ClickTaleEvent &&
                window.ClickTaleStop &&
                window.ClickTaleUploadPage &&
                window.ClickTaleLogical);
    },

    exec: function (data) {
      var mode = data.isProduction ? 'MM_Prod_' : 'MM_Sand_',
          campaignInfo = mode + data.campaignInfo,
          isDefault = ((campaignInfo.match(/\:/g) || []).length ===
                      (campaignInfo.match(/\:Default/g) || []).length);

      this.persist = this.persist ?
                          this.persist + ' - ' + campaignInfo :
                          campaignInfo;

      if (ClickTaleIsRecording() && !isDefault) {
        ClickTaleStop();
        window.ClickTaleIncludedOnDomReady = true;
        window.ClickTaleIncludedOnWindowLoad = true;
        ClickTaleUploadPage();
        ClickTaleLogical(window.location.href);
      }

      ClickTaleEvent(campaignInfo);
      ClickTaleEvent(this.persist);

      if(data.callback) data.callback();
      return true;
    }
  }
);