# ClickTale

---

[TOC]

## Overview
This integration is __v2.0.0__ integration with the ClickTale. It is used for extracting campaign experience data from the Maxymiser platform and sending it to ClickTale.

__NOTE:__ this version doesn't require any additional implementation on the ClickTale side.

### How we send the data

The integration with the ClickTale is performed by using the ClickTale API methods. When the integration is initialized by the campaign script, ClickTale recording is restarted and the campaign data is being sent to the ClickTale server (by triggering custom ClickTale event).

Please consider the exact integration steps below (for more info see the source code of the register script):

+ Wait for the required ClickTale API methods

```javascript
    check: function (data) {
        return (window.ClickTaleEvent &&
                window.ClickTaleStop &&
                window.ClickTaleUploadPage &&
                window.ClickTaleLogical);
    },
```

+ Get the campaign data

```javascript
var mode = data.isProduction ? 'MM_Prod_' : 'MM_Sand_',
    campaignInfo = mode + data.campaignInfo;
```

+ Concatenate the multiple campaigns' data and store it into the page persist storage

```javascrpt
this.persist = this.persist ?
                    this.persist + ' | ' + campaignInfo :
                    campaignInfo;
```

+ If the campaign variants __are NOT Default__ --> stop ClickTale recording and perform some ClickTale required stuff (can guess, but not sure what exactly is going on here)

```javascript
ClickTaleStop();
window.ClickTaleIncludedOnDomReady = true;
window.ClickTaleIncludedOnWindowLoad = true;
ClickTaleUploadPage();
ClickTaleLogical(window.location.href);
```

+ Send campaign data to the ClickTale by triggering `ClickTaleEvent`

```javascript
ClickTaleEvent(campaignInfo);
ClickTaleEvent(this.persist);
```

###Data Format
The data sent to ClickTale will be in the following format:

`<mode>_<campaign name>=<element1>:<variant>|<element2>:<variant>`

Please find additional information on the format of the sent data [here](https://bitbucket.org/mm-global-se/integration-factory#markdown-header-introduction
)

By default on each page the data is concatenated for multiple campaigns so to stop each campaign data replacing previous data that was sent.

##Prerequisite
The following information needs to be provided by the client:

+ Campaign Name

+ Latest version of ClickTale

+ Integration functionality to be turned on in ClickTale - please contact your account administrator at ClickTale to organise this


## Download

* [clicktale-register.js](https://bitbucket.org/mm-global-se/if-clicktale/src/master/src/clicktale-register.js)

* [clicktale-initialize.js](https://bitbucket.org/mm-global-se/if-clicktale/src/master/src/clicktale-initialize.js)


## Deployment instructions

### Content Campaign

+ Ensure that you have the Integration Factory plugin deployed on site level([find out more](https://bitbucket.org/mm-global-se/integration-factory)). Map this script to the whole site with an _output order: -10_

+ Create another site script and add the [clicktale-register.js](https://bitbucket.org/mm-global-se/if-clicktale/src/master/src/clicktale-register.js) script.  Map this script to the whole site with an _output order: -5_

    __NOTE!__ Please check whether the integration already exists on site level. Do not duplicate the Integration Factory plugin and the ClickTale Register scripts!

+ Create a campaign script and add the [clicktale-initialize.js](https://bitbucket.org/mm-global-se/if-clicktale/src/master/src/clicktale-initialize.js) script. Customise the code by changing the campaign name. Map this script to the page where the variants are generated on, with an _output order: 0_

### Redirect Campaign

For redirect campaigns, the campaign script needs to be mapped to both the generation and redirected page. To achieve this follow the steps bellow:

+ Go through the instructions outlined in [Content Campaign](https://bitbucket.org/mm-global-se/if-clicktale#markdown-header-content-campaign)

+ Make sure that in the initialize campaign script, redirect is set to true.

+ Create and map an additional page where the redirect variant would land on.

+ Map the integration initialize campaign script to this page as well.

## Check Recording In ClickTale UI

For this method you need to have corresponding account details for ClickTale UI

+ Load the test page

  (Please note: A ClickTale account can be configured so that the visitor session is not recorded on every page load - please ask the client/ClickTale account administrator to double check the configuration)

+ Interact with the page for a few minutes

+ Go to [ClickTale UI](http://www.clicktale.com/)

+ Select the target project from the project

    ![ct1.png](https://bitbucket.org/repo/8ydddB/images/2408195285-ct1.png)

+ Note - if the campaign is for mobile only, in the ClickTale UI you will need to switch to mobile traffic. Once you have selected the projects there should be a an icon to toggle between desktop and mobile on the right hand side

    ![ct1.png](https://bitbucket.org/repo/8ydddB/images/2408195285-ct1.png)

+ Search for recordings from the "Dashboard" page

    ![ct2.png](https://bitbucket.org/repo/8ydddB/images/3726685576-ct2.png)

+ Filter and select sessions from the search results list

    ![ct3.png](https://bitbucket.org/repo/8ydddB/images/4046289930-ct3.png)

+ Select and play the session

    ![ct4.png](https://bitbucket.org/repo/8ydddB/images/4028517672-ct4.png)